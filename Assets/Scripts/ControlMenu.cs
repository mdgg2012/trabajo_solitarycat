﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ControlMenu : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("SolitaryCat");
    }

    public void Salir()
    {
        Debug.Log("Ya salí del juego");
        Application.Quit();
    }
}
