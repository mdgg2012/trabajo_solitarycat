﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class PostProcesing : MonoBehaviour
{
    private Bloom bloom = null;
    void Start()
    {
        Volume volume = GetComponent<Volume>();

        volume.sharedProfile.TryGet<Bloom>(out bloom);
        if (bloom != null)
        {
            bloom.intensity.value = 1.0f;
        }
    }
}
