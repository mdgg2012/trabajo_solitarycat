﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuracion : MonoBehaviour
{

    void Start()
    {
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("Resolucion")))
        {
            PlayerPrefs.SetString("Resolucion", "HD");
        }
        Debug.Log("La resolucion es: " + PlayerPrefs.GetString("Resolucion"));
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            PlayerPrefs.SetString("Resolution", "HD");
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            PlayerPrefs.SetString("Resolution", "FullHD");
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            PlayerPrefs.SetString("Resolution", "4K");
        }
    }
}
